console.log("Hello World");

//Functions
	// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
	// Functions are mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

// Function declarations
	//(function statement) defines a function with the specified parameters.

	/*
	Syntax:
		function functionName() {
			code block (statement)
		}
	*/
	// function keyword - used to defined a javascript functions
	// functionName - the function name. Functions are named to be able to use later in the code.
	// function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.

function printName(){
	console.log("My Name is John");
};

printName();
printName();

function printSing(){
	console.log("wag ka nang mawala");

};
printSing();

// compute();
// - results in an error, much like variables, we cannot invoke a function we have yet to define.

//Function declaration vs Function Expression


//Function Declaration
declaredFunction();
// declared functions can be hoisted. As long as the function has been defined.

//Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration.

function declaredFunction(){
	console.log	("Hello Worl form declaredFunction()");
}
//declaredFunction();

//Function Expression

//A function can also be stored in a variable. This is called a function expression.

let variableFunction = function(){
	console.log("Hello Again");
}
variableFunction();

// We can also create a function expression of a named function.
// However, to invoke the function expression, we invoke it by its variable name, not by its function name.
// Function Expressions are always invoked (called) using the variable name.

		let	funcExpression = function funcName(){
			console.log	("Hello from the other side");
		}

		funcExpression();

		//funcName(); //not defined

		//You can reassign declared functions and function expressions to new anonymous functions.

		declaredFunction = function(){
			console.log	("updated declaredFunction");
		}
		declaredFunction();

		funcExpression	 = function(){
			console.log	("updated funcExpression");
		}
		funcExpression	();


		const constantFunc = function () {
			console.log("Initialized with const!")
		}
		constantFunc();
/*
		//constantFunc = function (){
	//console.log	("Cannot be re-assigned");
		}; // result an error		
		
*/

		//Function Scoping

		/*
		Scope is the accessibility (visibility) of variables within our program

		JavaScript variables has 3 types of scope
		1. local/block scope
		2.global scope
		3.function scope

		*/

			{
				let localVar = "Armando Perez";
				console.log	(localVar);
			}

			let globalVar = "Mr. Worldwide";

			console.log(globalVar);
			//console.log	(localVar); an error
			//result in error. localVar, being in a block, cannot be accessed outside of its code block.

	//Function Scope

			/*		
		JavaScript has function scope: Each function creates a new scope.
		Variables defined inside a function are not accessible (visible) from outside the function.
		Variables declared with var, let and const are quite similar when declared inside a function.
*/
			function showNames(){

				//Function scope variables
				var functionVar = "Joe";
				const functionConst = "John";
				let functionlet = "Jane";

				console.log(functionVar);
				console.log(functionConst);
				console.log(functionlet);
			}

			showNames();


				//console.log(functionVar); // result to an error
				//console.log(functionConst); // result to an error
				//console.log(functionlet); // result to an error

	//Nested Function
			//You can create another function inside a function
			//this is called a nested function

		console.log("-------------");	

			function myNewFunction(){
				let name = "Jane";

				function nestedFunction(){
					let nestedName ="John";
					console.log(name);

					}
					//console.log(nestedName); // error
					nestedFunction();
			}

			myNewFunction();
			//nestedFunction(); //an error


	// [SECTION] Using Alert()

			// alert() allows us to show a small window at the top of our browser page to show information to our users.
	// It allows us to show a short dialog or instructions to our users. The page will wait until the user dismisses the dialog.

function showSampleAlert(){
	alert("Hellow User");
}
showSampleAlert();

console.log("Hello, is it me you're looking for");
// This will run after an alert message

// Notes on the use of alert():
		// Show only an alert() for short dialogs/messages to the user.
		// Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.


		//[SECTION] Using prompt();
			// prompt() allow us to show small window at the top of the browser to gather user input.
	// The input form the prompt() will be returned as a "String" once the user dismisses the window.
	/*
		Syntax:

			let variableName = prompt("<dialogInString>");

	*/

		//let samplePromt = prompt("Enter you Full NAme");
		//console.log("Hello, " + samplePromt);

		function printWelcomMessage() {
			let firstName = prompt ("Enter yout First Name");
			let lastName = prompt ("Enter yout Last Name");
		
			console.log(`Hello, ${firstName} ${lastName}!`);
			console.log("Welcome to my Page!");
		}
		printWelcomMessage();

		printSing();

		// [SECTION] Function Naming Convention
	
	// Functions name should be definitive of the task it will perform. It usually contains a verb
function getCourses(){
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}

	getCourses();
//Avoide generic names to avoid confusion within our code.

	function get(){
		let name = "Jamie"
		console.log(name);
	}

	get();
// Avoid pointless and inappropriate function names, example: foo, bar, etc.
		// This are "metasyntactic variable" which are set of words identified as a placeholder in computer programming.

	function foo(){
		console.log(25%5);
	}

	foo();
// Name your functions in small caps. Follow camelCase when naming variables and functions.

	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000");
	}

	displayCarInfo();