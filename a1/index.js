/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	

function showName() {
	let firstName = prompt("Enter your First Name");
	let lastName = prompt("Enter your Last Name");

	console.log(`Hello, ${firstName} ${lastName}!`);
}
showName();

function showAge() {
	let Age = prompt("Enter your Age");


	console.log(`You are ${Age} years old.`);
}
showAge();

function showCity() {
	let City = prompt("Enter your City");


	console.log(`You live in ${City} City.`);
}
showCity();

function showAlert(){
	alert("Thank you for your input");
}
showAlert();

function showBandNames() {
console.log("1. The Beatles");
console.log("2. Metallica");
console.log("3. The Eagles");
console.log("4. L'arc~enCiel");
console.log("5. Eraseheads");

}
showBandNames();





function showMovies() {
	
	console.log("1. The Godfather");
	console.log("Rotten Tomatoes Rating: 97%");
	console.log("2. The Godfather, Part II");
	console.log("Rotten Tomatoes Rating: 96%");
	console.log("3. Shawshank Redemption");
	console.log("Rotten Tomatoes Rating: 91%");
	console.log("4. To Kill A Mockingbird");
	console.log("Rotten Tomatoes Rating: 93%");
	console.log("5. Psycho");
	console.log("Rotten Tomatoes Rating: 96%");
}
showMovies();















	
	function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(`1. ` + friend1);
	console.log(`2. ` + friend2);
	console.log(`3. ` + friend3);
}

printFriends();